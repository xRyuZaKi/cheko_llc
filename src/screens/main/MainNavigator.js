import React, {Component} from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';

import SplashScreen from '../welcome/SplashScreen';
import NotificationScreen from '../notification/NotificationScreen';
import Dashboard from '../dashboard/Dashboard';

const RootStack = createStackNavigator(
  {
    SplashScreen: SplashScreen,
    NotificationScreen: NotificationScreen,
    Dashboard: Dashboard,
  },
  {
    initialRouteName: 'SplashScreen',
  },
);

const AppContainer = createAppContainer(RootStack);

export default class MainNavigator extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return <AppContainer />;
  }
}
