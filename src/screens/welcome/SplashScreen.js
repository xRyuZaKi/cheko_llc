/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, Image} from 'react-native';

import {styles_splash_screen} from '../../helpers/Style';
import Text from '../../component/Text';

const TIME_OUT = 3000;

export default class SplashScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('NotificationScreen');
    }, TIME_OUT);
  }

  render() {
    return (
      <View style={styles_splash_screen.container}>
        <Text style={styles_splash_screen.text}>Welcome To</Text>
        <Image
          source={require('../../../assets/images/logo.png')}
          style={styles_splash_screen.logo}
        />
      </View>
    );
  }
}
