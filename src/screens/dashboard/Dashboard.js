/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';

import {Image} from 'react-native';

import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createAppContainer} from 'react-navigation';

import LogoTitle from '../../component/Header';

import HomeScreen from './home/HomeScreen';
import CategorieScreen from './home/CategorieScreen';
import DetailProductScreen from "./home/DetailProductScreen";

import ReviewScreen from './review/ReviewScreen';
import DiscoverScreen from './review/DiscoverScreen';

import CartScreen from './cart/CartScreen';
import CouponScreen from './cart/CouponScreen';

import FavoriteScreen from './favorite/FavoriteScreen';
import ProfileScreen from './profile/ProfileScreen';

const color_header = '#000000';
const color_footer = '#FFF7F8';
const height_icon = 25;
const width_icon = 25;

const navigationOptions = {
  headerLeft: null,
  headerTitle: <LogoTitle />,
  headerStyle: {
    backgroundColor: color_header,
  },
};

const HomeTab = createStackNavigator(
  {
    Home: HomeScreen,
    Categorie: CategorieScreen,
    DetailProduct: DetailProductScreen
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const ReviewTab = createStackNavigator(
  {
    Review: ReviewScreen,
    Discover: DiscoverScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const CartTab = createStackNavigator(
  {
    Cart: CartScreen,
    Coupon: CouponScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const FavoriteTab = createStackNavigator(
  {
    Favorite: FavoriteScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const ProfileTab = createStackNavigator(
  {
    Profile: ProfileScreen,
  },
  {
    defaultNavigationOptions: navigationOptions,
  },
);

const App = createBottomTabNavigator(
  {
    Home: {
      screen: HomeTab,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Image
            source={require('../../../assets/images/home_icons/1.png')}
            style={{height: height_icon, width: width_icon}}
          />
        ),
      },
    },
    Review: {
      screen: ReviewTab,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Image
            source={require('../../../assets/images/home_icons/2.png')}
            style={{height: height_icon, width: width_icon}}
          />
        ),
      },
    },
    Cart: {
      screen: CartTab,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Image
            source={require('../../../assets/images/home_icons/3.png')}
            style={{height: height_icon, width: width_icon}}
          />
        ),
      },
    },
    Favorite: {
      screen: FavoriteTab,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Image
            source={require('../../../assets/images/home_icons/4.png')}
            style={{height: height_icon, width: width_icon}}
          />
        ),
      },
    },
    Profile: {
      screen: ProfileTab,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Image
            source={require('../../../assets/images/home_icons/5.png')}
            style={{height: height_icon, width: width_icon}}
          />
        ),
      },
    },
  },
  {
    tabBarOptions: {
      activeTintColor: '#A2A2A9',
      inactiveTintColor: '#FFF7F8',
      style: {
        backgroundColor: color_footer,
      },
      showIcon: true,
      showLabel: false,
    },
  },
);

const AppContainer = createAppContainer(App);

export default class Dashboard extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return <AppContainer />;
  }
}
