/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, Image, Dimensions, ScrollView} from 'react-native';
import {
  Container,
  Header,
  Card,
  CardItem,
  Body,
  Content,
  Left,
  Right,
  Button,
} from 'native-base';

import Text from '../../../component/Text';

export default class ReviewScreen extends Component {
  render() {
    const uri =
      'https://cdn.shopify.com/s/files/1/0101/1725/1130/products/4274ca52-edb6-4a89-8434-9ce678772a77_large.jpg?v=1561514876';
    const uri_2 =
      'https://cdn.shopify.com/s/files/1/0101/1725/1130/products/a9be896f390b483463e29a5e2f67dda2_large.png?v=1544768305';

    const win = Dimensions.get('window');

    return (
      <Container>
        <Header style={{backgroundColor: '#FFF7F8'}}>
          <Left style={{flex: 1}} />
          <Body style={{flex: 1, marginRight: 5}}>
            <Button
              rounded
              block
              bordered
              style={{backgroundColor: '#FFF7F8'}}
              disabled={true}>
              <Text style={{color: '#000000'}}>My Feed</Text>
            </Button>
          </Body>
          <Body style={{flex: 1, marginLeft: 5}}>
            <Button
              rounded
              block
              onPress={() => this.props.navigation.navigate('Discover')}
              style={{backgroundColor: '#000000'}}>
              <Text style={{color: '#FFF7F8'}}>Discover</Text>
            </Button>
          </Body>
          <Right style={{flex: 1}} />
        </Header>
        <ScrollView>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 1, height: 300}}>
              <Card>
                <CardItem cardBody>
                  <Image
                    source={{uri: uri}}
                    style={{height: 245, width: null, flex: 1}}
                  />
                </CardItem>
                <CardItem style={{backgroundColor: '#000000'}}>
                  <Left>
                    <Text style={{color: '#FFF7F8'}}>12 Likes</Text>
                  </Left>
                  <Right>
                    <Text style={{color: '#FFF7F8'}}>11h ago</Text>
                  </Right>
                </CardItem>
              </Card>
            </View>
            <View style={{flex: 1, height: 300}}>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <Card>
                  <CardItem cardBody>
                    <Image
                      source={{uri: uri}}
                      style={{height: 96.3, width: null, flex: 1}}
                    />
                  </CardItem>
                  <CardItem style={{backgroundColor: '#000000'}}>
                    <Left>
                      <Text style={{color: '#FFF7F8'}}>12 Likes</Text>
                    </Left>
                    <Right>
                      <Text style={{color: '#FFF7F8'}}>11h ago</Text>
                    </Right>
                  </CardItem>
                </Card>

                <Card>
                  <CardItem cardBody>
                    <Image
                      source={{uri: uri}}
                      style={{height: 96.3, width: null, flex: 1}}
                    />
                  </CardItem>
                  <CardItem style={{backgroundColor: '#000000'}}>
                    <Left>
                      <Text style={{color: '#FFF7F8'}}>12 Likes</Text>
                    </Left>
                    <Right>
                      <Text style={{color: '#FFF7F8'}}>11h ago</Text>
                    </Right>
                  </CardItem>
                </Card>
              </View>
            </View>
          </View>

          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 1, height: 300}}>
              <View style={{flex: 1, flexDirection: 'column'}}>
                <Card>
                  <CardItem cardBody>
                    <Image
                      source={{uri: uri}}
                      style={{height: 96.3, width: null, flex: 1}}
                    />
                  </CardItem>
                  <CardItem style={{backgroundColor: '#000000'}}>
                    <Left>
                      <Text style={{color: '#FFF7F8'}}>12 Likes</Text>
                    </Left>
                    <Right>
                      <Text style={{color: '#FFF7F8'}}>11h ago</Text>
                    </Right>
                  </CardItem>
                </Card>

                <Card>
                  <CardItem cardBody>
                    <Image
                      source={{uri: uri}}
                      style={{height: 96.3, width: null, flex: 1}}
                    />
                  </CardItem>
                  <CardItem style={{backgroundColor: '#000000'}}>
                    <Left>
                      <Text style={{color: '#FFF7F8'}}>12 Likes</Text>
                    </Left>
                    <Right>
                      <Text style={{color: '#FFF7F8'}}>11h ago</Text>
                    </Right>
                  </CardItem>
                </Card>
              </View>
            </View>

            <View style={{flex: 1, height: 300}}>
              <Card>
                <CardItem cardBody>
                  <Image
                    source={{uri: uri}}
                    style={{height: 245, width: null, flex: 1}}
                  />
                </CardItem>
                <CardItem style={{backgroundColor: '#000000'}}>
                  <Left>
                    <Text style={{color: '#FFF7F8'}}>12 Likes</Text>
                  </Left>
                  <Right>
                    <Text style={{color: '#FFF7F8'}}>11h ago</Text>
                  </Right>
                </CardItem>
              </Card>
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}
