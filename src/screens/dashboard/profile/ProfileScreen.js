import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableHighlight,
  Image,
  Alert,
  ScrollView,
} from 'react-native';

import Text from '../../../component/Text';

import {Col, Row, Grid} from 'react-native-easy-grid';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Body,
  Left,
  Right,
} from 'native-base';

export default class ProfileScreen extends Component {
  constructor(props) {
    super(props);
  }

  onClickListener = viewId => {
    Alert.alert('Alert', 'Button pressed ' + viewId);
  };

  render() {
    return (
      <Container>
        <Content>
          <Card transparent>
            <Grid>
              <Row>
                <Col>
                  <CardItem>
                    <Left />
                    <Body>
                      <Image
                        source={require('../../../../assets/images/home_icons/5.png')}
                        style={{width: 100, height: 100}}
                      />
                    </Body>
                    <Right />
                  </CardItem>
                </Col>
              </Row>
              <Row>
                <Col>
                  <CardItem>
                    <Left />
                    <Body>
                      <Button
                        block
                        rounded
                        onPress={() =>
                          this.props.navigation.navigate('Register')
                        }
                        style={{backgroundColor: '#FFF7F8'}}>
                        <Text style={{color: '#000000'}}>Sign In</Text>
                      </Button>
                    </Body>
                    <Right />
                  </CardItem>
                </Col>
              </Row>
              <Row style={{flex: 1}}>
                <Col style={{alignItems: 'center'}}>
                  <Text>3</Text>
                  <Text>Reviews</Text>
                </Col>
                <Col style={{alignItems: 'center'}}>
                  <Text>5</Text>
                  <Text>Followers</Text>
                </Col>
                <Col style={{alignItems: 'center'}}>
                  <Text>10</Text>
                  <Text>Following</Text>
                </Col>
              </Row>
              <Text>{'\n'}</Text>
              <Row>
                <Col>
                  <CardItem>
                    <Left />
                    <Body>
                      <Text>My orders (1)</Text>
                    </Body>
                    <Right />
                  </CardItem>
                </Col>
              </Row>
              <Row>
                <Col>
                  <CardItem>
                    <Left />
                    <Body>
                      <Text>Notifications (3)</Text>
                    </Body>
                    <Right />
                  </CardItem>
                </Col>
              </Row>
              <Row>
                <Col>
                  <CardItem>
                    <Left />
                    <Body>
                      <Text>Promo Code</Text>
                    </Body>
                    <Right />
                  </CardItem>
                </Col>
              </Row>
              <Row>
                <Col>
                  <CardItem>
                    <Left />
                    <Body>
                      <Text>My Addresses</Text>
                    </Body>
                    <Right />
                  </CardItem>
                </Col>
              </Row>
              <Row>
                <Col>
                  <CardItem>
                    <Left />
                    <Body>
                      <Text>Costumer Support</Text>
                    </Body>
                    <Right />
                  </CardItem>
                </Col>
              </Row>
            </Grid>
          </Card>
        </Content>
      </Container>
    );
  }
}
