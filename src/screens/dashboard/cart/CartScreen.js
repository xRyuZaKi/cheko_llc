import React, {Component} from 'react';
//import { Image, PermissionsAndroid, Platform } from "react-native";
import {
  Image,
  Linking,
  TouchableHighlight,
  PermissionsAndroid,
  Platform,
  StyleSheet,
  View,
} from 'react-native';
// import all basic components
import {CameraKitCameraScreen} from 'react-native-camera-kit';
import {
  Container,
  Header,
  Left,
  CheckBox,
  Content,
  ListItem,
  Body,
  Card,
  CardItem,
  Thumbnail,
  Right,
  Badge,
  Button,
} from 'native-base';
import {Col, Row, Grid} from 'react-native-easy-grid';

import Text from '../../../component/Text';

export default class CartScreen extends Component {
  constructor() {
    super();
    this.state = {
      //variable to hold the qr value
      qrvalue: '',
      opneScanner: false,
    };
  }

  componentDidMount() {
    const {navigation} = this.props;
    navigation.addListener('willFocus', () =>
      this.setState({opneScanner: false}),
    );
    navigation.addListener('willBlur', () =>
      this.setState({opneScanner: false}),
    );
  }

  onOpneScanner() {
    var that = this;
    //To Start Scanning
    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
              title: 'CameraExample App Camera Permission',
              message: 'CameraExample App needs access to your camera ',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //If CAMERA Permission is granted
            that.setState({qrvalue: ''});
            that.setState({opneScanner: true});
          } else {
            alert('CAMERA permission denied');
          }
        } catch (err) {
          alert('Camera permission err', err);
          console.warn(err);
        }
      }
      //Calling the camera permission function
      requestCameraPermission();
    } else {
      that.setState({qrvalue: ''});
      that.setState({opneScanner: true});
    }
  }

  onBarcodeScan(qrvalue) {
    //called after te successful scanning of QRCode/Barcode
    /*this.setState({ qrvalue: qrvalue });*/
    this.setState({opneScanner: false});
    this.props.navigation.navigate('Coupon', {valueQR: qrvalue});
  }

  render() {
    const uri =
      'https://cdn.shopify.com/s/files/1/0101/1725/1130/products/072a80f7b9fea5e607af38b84f92a1a7_large.png?v=1544768297';

    if (this.state.opneScanner) {
      return (
        <View style={{flex: 1}}>
          <CameraKitCameraScreen
            showFrame={false}
            //Show/hide scan frame
            scanBarcode={true}
            //Can restrict for the QR Code only
            laserColor={'blue'}
            //Color can be of your choice
            frameColor={'yellow'}
            //If frame is visible then frame color
            colorForScannerFrame={'black'}
            //Scanner Frame color
            onReadCode={event =>
              this.onBarcodeScan(event.nativeEvent.codeStringValue)
            }
          />
        </View>
      );
    }

    return (
      <Container>
        <Header style={{backgroundColor: '#FFF7F8'}}>
          <Left>
            <Button
              rounded
              block
              bordered
              onPress={() => this.onOpneScanner()}
              style={{backgroundColor: '#000000'}}>
              <Text style={{color: '#FFF7F8'}}>Enter Promo Code</Text>
            </Button>
          </Left>
        </Header>
        <Content>
          <Card transparent>
            <Grid>
              <Row>
                <Col>
                  <CardItem>
                    <Left>
                      <CheckBox checked={true} />
                      <Body>
                        <Text>
                          {'\u00A0'}
                          {'\u00A0'}Avocado BackPack
                        </Text>
                      </Body>
                    </Left>
                  </CardItem>
                </Col>
              </Row>
              <Row style={{flex: 1}}>
                <Col>
                  <CardItem cardBody>
                    <Image
                      source={{uri: uri}}
                      style={{
                        resizeMode: 'contain',
                        height: 120,
                        width: 120,
                        flex: 1,
                      }}
                    />
                  </CardItem>
                </Col>
                <Col>
                  <Row>
                    <Col>
                      <CardItem>
                        <Left>
                          <Body>
                            <Text>$50.00</Text>
                            <Text>$25.00</Text>
                          </Body>
                        </Left>
                      </CardItem>
                    </Col>
                    <Col>
                      <CardItem>
                        <Left>
                          <Text>-</Text>
                        </Left>
                        <Body>
                          <Text>2</Text>
                        </Body>
                        <Right>
                          <Text>+</Text>
                        </Right>
                      </CardItem>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <CardItem>
                        <Left>
                          <Body>
                            <Text>Like the picture. 1pc.</Text>
                            <Text>Shipping: 25 days, $0</Text>
                          </Body>
                        </Left>
                      </CardItem>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row>
                <Col />
                <Col>
                  <Button
                    block
                    rounded
                    onPress={() => this.props.navigation.navigate('Coupon')}
                    style={{backgroundColor: '#FFF7F8'}}>
                    <Text style={{color: '#000000'}}>Buy</Text>
                  </Button>
                </Col>
                <Col />
              </Row>
            </Grid>
          </Card>
        </Content>
      </Container>
    );
  }
}
