/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {ImageBackground, Dimensions, View} from 'react-native';
import Text from '../../../component/Text';
import {
  Container,
  Content,
  Button,
  Header,
  Left,
  Card,
  CardItem,
  Body,
} from 'native-base';
import {Col, Row, Grid} from 'react-native-easy-grid';

export default class CouponScreen extends Component {
  render() {
    const uri =
      'https://cdn.shopify.com/s/files/1/0101/1725/1130/products/fccf089a0a877e429388b1aea99dab4e_large.png?v=1544768405';

    const win = Dimensions.get('window');
    const {navigation} = this.props;
    const valueQR = navigation.getParam('valueQR', 'NO-ID');

    return (
      <Container>
        <Header style={{backgroundColor: '#FFF7F8'}}>
          <Left>
            <Button rounded block bordered style={{backgroundColor: '#000000'}}>
              <Text style={{color: '#FFF7F8'}}>Don't use Coupon</Text>
            </Button>
          </Left>
        </Header>
        <Content>
          <Card transparent>
            <Grid>
              <Row>
                <Col>
                  <CardItem>
                    <Body>
                      <Text>Value QR = {valueQR}</Text>
                    </Body>
                  </CardItem>
                </Col>
              </Row>
            </Grid>
          </Card>
        </Content>
      </Container>
    );
  }
}
