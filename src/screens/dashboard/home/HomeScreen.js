/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Image, TouchableOpacity, FlatList, View} from 'react-native';

import {
  Container,
  Header,
  Left,
  Button,
  Right,
  Content,
  Body,
  Card,
  CardItem,
} from 'native-base';

import Text from '../../../component/Text';
import {styles_background} from '../../../helpers/Style';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      GridListItems: [
        {
          key: 'Skptricks',
          uri:
            'https://cdn.shopify.com/s/files/1/0101/1725/1130/products/4274ca52-edb6-4a89-8434-9ce678772a77_large.jpg?v=1561514876',
        },
        {
          key: 'Sumit',
          uri:
            'https://cdn.shopify.com/s/files/1/0101/1725/1130/products/a9be896f390b483463e29a5e2f67dda2_large.png?v=1544768305',
        },
        {
          key: 'Amit',
          uri:
            'https://cdn.shopify.com/s/files/1/0101/1725/1130/products/072a80f7b9fea5e607af38b84f92a1a7_large.png?v=1544768297',
        },
        {
          key: 'React',
          uri:
            'https://cdn.shopify.com/s/files/1/0101/1725/1130/products/fccf089a0a877e429388b1aea99dab4e_large.png?v=1544768405',
        },
        {
          key: 'React_6',
          uri:
            'https://cdn.shopify.com/s/files/1/0101/1725/1130/products/fccf089a0a877e429388b1aea99dab4e_large.png?v=1544768405',
        },
        {
          key: 'React_7',
          uri:
            'https://cdn.shopify.com/s/files/1/0101/1725/1130/products/fccf089a0a877e429388b1aea99dab4e_large.png?v=1544768405',
        },
        {
          key: 'React_8',
          uri:
            'https://cdn.shopify.com/s/files/1/0101/1725/1130/products/fccf089a0a877e429388b1aea99dab4e_large.png?v=1544768405',
        },
        {
          key: 'React_9',
          uri:
            'https://cdn.shopify.com/s/files/1/0101/1725/1130/products/fccf089a0a877e429388b1aea99dab4e_large.png?v=1544768405',
        },
      ],
    };
  }

  GetGridViewItem(item) {
    this.props.navigation.navigate('DetailProduct');
  }

  render() {
    return (
      <Container style={styles_background.container}>
        <Header style={{backgroundColor: '#FFF7F8', alignSelf: 'center'}}>
          <Left style={{flex: 1}} />
          <Body style={{flex: 1, marginRight: 5}}>
            <Button
              rounded
              block
              bordered
              style={{backgroundColor: '#FFF7F8'}}
              onPress={() => this.props.navigation.navigate('Home')}
              disabled={true}>
              <Text style={{color: '#000000'}}>Products</Text>
            </Button>
          </Body>
          <Body style={{flex: 1, marginLeft: 5}}>
            <Button
              rounded
              block
              style={{backgroundColor: '#000000'}}
              onPress={() => this.props.navigation.navigate('Categorie')}>
              <Text style={{color: '#FFF7F8'}}>Categories</Text>
            </Button>
          </Body>
          <Right style={{flex: 1}} />
        </Header>
        <Content style={{flex: 1}}>
          <FlatList
            data={this.state.GridListItems}
            renderItem={({item}) => (
              <Content>
                <Card>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('DetailProduct', {
                        nameProduct: item.key,
                        uri: item.uri,
                      })
                    }>
                    <CardItem cardBody>
                      <Image
                        source={{uri: item.uri}}
                        style={{
                          height: 150,
                          width: null,
                          flex: 1,
                          resizeMode: 'contain',
                        }}
                      />
                    </CardItem>
                    <CardItem style={{backgroundColor: '#000000'}}>
                      <Left />
                      <Body>
                        <Text style={{color: '#FFF7F8'}}>$30</Text>
                      </Body>
                      <Body style={styles.TextViewStyle}>
                        <Text style={styles.TextStyle}>50%</Text>
                      </Body>
                      <Right />
                    </CardItem>
                  </TouchableOpacity>
                </Card>
              </Content>
            )}
            numColumns={2}
          />
        </Content>
      </Container>
    );
  }
}

const styles = {
  container: {
    marginTop: 3,
    marginLeft: 3,
    marginRight: 3,
    marginBottom: 3,
  },
  TextViewStyle: {
    borderWidth: 1,
    borderRadius: 10,
    //padding: 5,
    alignItems: 'center',
    backgroundColor: '#FFF7F8',
  },
  TextStyle: {
    textAlign: 'center',
    color: '#000000',
  },
};
