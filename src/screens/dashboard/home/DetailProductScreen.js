import React, {Component} from 'react';
import {StyleSheet, View, Image, Alert, ScrollView} from 'react-native';

import {Button, Card, CardItem, Left, Body, Right} from 'native-base';
import Text from '../../../component/Text';

export default class DetailProductScreen extends Component {
  constructor(props) {
    super(props);
  }

  clickEventListener() {
    Alert.alert('Success', 'Product has beed added to cart');
  }

  render() {
    const {navigation} = this.props;
    const nameProduct = navigation.getParam('nameProduct', 'NO-ID');
    const uri = navigation.getParam('uri', 'some default value');

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{alignItems: 'center', marginHorizontal: 30}}>
            <Image
              style={styles.productImg}
              source={{
                uri: uri,
              }}
            />
            <View style={{flex: 1}}>
              <Text />
              <Text style={styles.description}>$50.00</Text>
              <Text style={styles.description}>$25.00</Text>
              <Text style={styles.description}>Like the picture. 1 pc.</Text>
              <Text style={styles.description}>Shipping: 25 days, $0</Text>
              <Text style={styles.description}>Rate: 5</Text>
            </View>
          </View>
          <Text />
          <Card transparent>
            <CardItem>
              <Left />
              <Body style={{flex: 1}}>
                <Button rounded block style={{backgroundColor: '#000000'}}>
                  <Text style={{color: '#FFF7F8'}}>Buy</Text>
                </Button>
              </Body>
              <Right />
            </CardItem>
          </Card>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  productImg: {
    width: 200,
    height: 200,
  },
  name: {
    fontSize: 28,
    color: '#696969',
    fontWeight: 'bold',
  },
  price: {
    marginTop: 10,
    fontSize: 18,
    color: 'green',
    fontWeight: 'bold',
  },
  description: {
    textAlign: 'left',
    color: '#696969',
  },
  star: {
    width: 40,
    height: 40,
  },
  btnColor: {
    height: 30,
    width: 30,
    borderRadius: 30,
    marginHorizontal: 3,
  },
  btnSize: {
    height: 40,
    width: 40,
    borderRadius: 40,
    borderColor: '#778899',
    borderWidth: 1,
    marginHorizontal: 3,
    backgroundColor: 'white',

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starContainer: {
    justifyContent: 'center',
    marginHorizontal: 30,
    flexDirection: 'row',
    marginTop: 20,
  },
  contentColors: {
    justifyContent: 'center',
    marginHorizontal: 30,
    flexDirection: 'row',
    marginTop: 20,
  },
  contentSize: {
    justifyContent: 'center',
    marginHorizontal: 30,
    flexDirection: 'row',
    marginTop: 20,
  },
  separator: {
    height: 2,
    backgroundColor: '#eeeeee',
    marginTop: 20,
    marginHorizontal: 30,
  },
  shareButton: {
    marginTop: 10,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: '#00BFFF',
  },
  shareButtonText: {
    color: '#FFFFFF',
    fontSize: 20,
  },
  addToCarContainer: {
    marginHorizontal: 30,
  },
});
