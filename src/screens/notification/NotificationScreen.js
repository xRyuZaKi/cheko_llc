/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, Image} from 'react-native';
import {Button} from 'native-base';

import {styles_notification} from '../../helpers/Style';
import Text from '../../component/Text';

export default class NotificationScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  goToDashboard() {
    this.props.navigation.navigate('Dashboard');
  }

  render() {
    return (
      <View style={styles_notification.container}>
        <View style={{justifyContent: 'center', marginTop: 3, flex: 0.3}}>
          <Image
            source={require('../../../assets/images/logo.png')}
            style={styles_notification.logo}
          />
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 1,
            flex: 0.1,
          }}>
          <Text style={styles_notification.text}>Don't miss anything</Text>
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 1,
            flex: 0.2,
          }}>
          <Text style={styles_notification.text}>We inform you about the</Text>
          <Text style={styles_notification.text}>
            best offers reductions, as
          </Text>
          <Text style={styles_notification.text}>well as arrival of your</Text>
          <Text style={styles_notification.text}>
            orders at the delivery point.
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 0.3,
          }}>
          <Button
            rounded
            block
            style={{backgroundColor: '#FFF7F8'}}
            onPress={() => this.goToDashboard()}>
            <Text style={styles_notification.text_button}>
              Activate Notifications
            </Text>
          </Button>
          <Text />
          <Text
            style={styles_notification.text}
            onPress={() => this.goToDashboard()}>
            I'm ok
          </Text>
        </View>
      </View>
    );
  }
}
