/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Image} from 'react-native';

export default class Header extends Component {
  render() {
    return (
      <Image
        source={require('../../assets/images/logo.png')}
        style={{width: 100, height: 30, marginLeft: 3}}
      />
    );
  }
}
