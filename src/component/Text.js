import React, {Component} from 'react';
import {Text as TextRN, StyleSheet} from 'react-native';

export default class Text extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TextRN style={[styles.defaultStyle, this.props.style]}>
        {this.props.children}
      </TextRN>
    );
  }
}

const styles = StyleSheet.create({
  // ... add your default style here
  defaultStyle: {
    fontFamily: 'Cabin-Regular',
    //fontFamily: 'VINCHAND',
  },
});
