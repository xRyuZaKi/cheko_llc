import {StyleSheet, Dimensions} from 'react-native';

const styles_splash_screen = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000000',
  },
  text: {
    color: '#FFF7F8',
  },
  logo: {
    width: 150,
    height: 50,
  },
});

const styles_notification = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000',
    alignItems: 'center',
    //justifyContent: "center"
  },
  text: {
    color: '#FFF7F8',
    textAlign: 'justify',
  },
  text_button: {
    color: '#000000',
  },
  logo: {
    width: 150,
    height: 50,
  },
});

const styles_background = StyleSheet.create({
  container: {
    backgroundColor: '#FFF7F8',
  },
  header: {
    backgroundColor: '#FFF7F8',
  },
});

export {styles_splash_screen, styles_notification, styles_background};
