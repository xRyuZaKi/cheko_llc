import React, {Component} from 'react';

import MainNavigator from '../src/screens/main/MainNavigator';

export default class Main extends Component {
  render() {
    return <MainNavigator />;
  }
}
